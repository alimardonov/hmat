package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Menu;
import uz.nuu.entity.Permission;

import java.util.HashMap;
import java.util.List;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {

    List<Menu> findAllByIdIn(List<Long> ids);

    Menu findByTitle(HashMap title);
}
