package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Students;

import java.util.UUID;

@Repository
public interface StudentsRepository extends JpaRepository<Students, UUID> {
}
