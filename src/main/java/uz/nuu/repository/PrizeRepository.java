package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Prizes;

import java.util.UUID;

@Repository
public interface PrizeRepository extends JpaRepository<Prizes, UUID> {
}
