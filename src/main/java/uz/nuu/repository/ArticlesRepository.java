package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.Articles;

import java.util.UUID;

@Repository
public interface ArticlesRepository extends JpaRepository<Articles, UUID> {
}
