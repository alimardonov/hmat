package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.FileDocuments;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface FileDocumentsRepository extends JpaRepository<FileDocuments, UUID> {

    Optional<FileDocuments> findByGeneratedName(String generatedName);
}
