package uz.nuu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.nuu.entity.FileAvatar;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface FileAvatarRepository  extends JpaRepository<FileAvatar,UUID>{
    Optional<FileAvatar> findByGeneratedName(String generatedName);
}
