package uz.nuu.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.entity.FileDocuments;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.exception.FileUploadException;
import uz.nuu.exception.InvalidDataException;

import java.net.MalformedURLException;
import java.util.List;
import java.util.UUID;

public interface FileDocumentsService {

    List<FileDocuments> documentsUpload(List<MultipartFile> multipartFiles, String documentType) throws FileUploadException, InvalidDataException;

    Resource getDocumentAsResourceById(UUID id) throws DataNotFoundException, MalformedURLException;

    Resource getDocumentAsResourceByGeneratedName(String generatedName) throws DataNotFoundException, MalformedURLException;

    FileDocuments getByGeneratedName(String docGeneratedName);

    UUID saveFile(String generationName, String originalName, String documentType);
}
