package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.ProductionProcess;
import uz.nuu.entity.Students;
import uz.nuu.entity.Teachers;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.ProductionProcessRepository;
import uz.nuu.service.ProductionProductService;

import java.util.List;
import java.util.UUID;

@Service
public class ProductionProcessServiceImpl implements ProductionProductService {
    private final ProductionProcessRepository productionProcessRepository;

    public ProductionProcessServiceImpl(ProductionProcessRepository productionProcessRepository) {
        this.productionProcessRepository = productionProcessRepository;
    }

    @Override
    public ProductionProcess create(ProductionProcess request) throws DataAlreadyExistsException {
        return productionProcessRepository.save(request);
    }

    @Override
    public ProductionProcess getById(UUID id) throws DataNotFoundException {
        return productionProcessRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public ProductionProcess update(UUID id, ProductionProcess request) throws DataNotFoundException, DataAlreadyExistsException {
        ProductionProcess productionProcess = getById(id);
        productionProcess.setCyAddress(request.getCyAddress());
        productionProcess.setCyName(request.getCyName());
        productionProcess.setCyPhoneNumber(request.getCyPhoneNumber());
        productionProcess.setDocumentationLink(request.getDocumentationLink());
        productionProcess.setEndDate(request.getEndDate());
        productionProcess.setStartDate(request.getStartDate());
        productionProcess.setRating(request.getRating());
        productionProcess.setStudents(request.getStudents());
        productionProcess.setTeachers(request.getTeachers());
        productionProcess.setTeacherId(request.getTeacherId());
        productionProcess.setStudentId(request.getStudentId());
        return productionProcessRepository.save(productionProcess);
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            productionProcessRepository.deleteById(id);
            return true;
        }
        return true;
    }

    @Override
    public List<ProductionProcess> list() throws ApiException {
        return productionProcessRepository.findAll();
    }
}
