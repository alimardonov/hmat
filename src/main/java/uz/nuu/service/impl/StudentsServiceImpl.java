package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Students;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.StudentsRepository;
import uz.nuu.service.StudentService;

import java.util.List;
import java.util.UUID;

@Service
public class StudentsServiceImpl implements StudentService {

    private final StudentsRepository repository;

    public StudentsServiceImpl(StudentsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Students create(Students request) throws DataAlreadyExistsException {
        return repository.save(request);
    }

    @Override
    public Students getById(UUID id) throws DataNotFoundException {
        return repository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public Students update(UUID id, Students request) throws DataNotFoundException, DataAlreadyExistsException {
        Students findStudent = getById(id);
        findStudent.setFirstName(request.getFirstName());
        findStudent.setLastName(request.getLastName());
        findStudent.setMiddleName(request.getMiddleName());
        findStudent.setAddress(request.getAddress());
        findStudent.setDirection(request.getDirection());
        findStudent.setCourse(request.getCourse());
        findStudent.setUrlImage(request.getUrlImage());
        findStudent.setReadingType(request.getReadingType());
        findStudent.setDegreeType(request.getDegreeType());
        findStudent.setEmail(request.getEmail());
        findStudent.setFacebook(request.getFacebook());
        findStudent.setTwitter(request.getTwitter());
        findStudent.setLinkedIn(request.getLinkedIn());
        findStudent.setInstagram(request.getInstagram());
        return repository.save(findStudent);
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        repository.deleteById(id);
        return true;
    }

    @Override
    public List<Students> list() throws ApiException {
        return repository.findAll();
    }
}
