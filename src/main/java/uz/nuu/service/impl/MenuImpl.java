package uz.nuu.service.impl;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.nuu.entity.Menu;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.MenuRepository;
import uz.nuu.service.MenuService;

import java.util.HashMap;
import java.util.List;

@Service
public class MenuImpl implements MenuService {

    private final MenuRepository menuRepository;

    public MenuImpl(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    @Transactional
    public Menu create(Menu request) throws DataAlreadyExistsException {
        if (menuRepository.findByTitle(request.getTitle()) == null) {
            return menuRepository.save(request);
        } else {
            throw new DataAlreadyExistsException("This data already exists");
        }

    }

    @Override
    @Transactional
    public Menu getById(Long id) throws DataNotFoundException {
        return menuRepository
                .findById(id)
                .orElseThrow(() -> new DataNotFoundException("Data not found id = " + id));
    }

    @Override
    public List<Menu> list() {
        return menuRepository.findAll();
    }

    @Override
    public Menu findById(Long id) throws DataNotFoundException {
        menuRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data id =" + id + "not found"));
        return menuRepository.findById(id).get();
    }

    @Override
    public Menu getByTitle(HashMap title) {
        return menuRepository.findByTitle(title);
    }

    @Override
    public Menu update(Long id, Menu request) throws DataNotFoundException, DataAlreadyExistsException {
        try {
            Menu menu = getById(id);
            if (getByTitle(request.getTitle()) != null) {
                throw new DataAlreadyExistsException("This data already exists");
            }
            menu.setTitle(request.getTitle());
            menu.setParentMenu(request.getParentMenu());
            return menuRepository.save(menu);
        } catch (ConstraintViolationException e) {
            throw new DataAlreadyExistsException("This data already exists");
        }
    }

    @Override
    public boolean delete(Long id) throws DataNotFoundException {
        if (getById(id) != null) {
            menuRepository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found id = " + id);
        }
    }
}
