package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Books;
import uz.nuu.entity.BooksType;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.BooksRequest;
import uz.nuu.repository.BooksRepository;
import uz.nuu.service.BooksService;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;

    public BooksServiceImpl(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @Override
    public Books create(Books request) throws DataAlreadyExistsException {
        return booksRepository.save(request);
    }

    @Override
    public Books getById(UUID id) throws DataNotFoundException {
        return booksRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found exception"));
    }

    @Override
    public Books update(UUID id, BooksRequest request, BooksType booksType) throws DataNotFoundException, DataAlreadyExistsException {
        Books findBooks = getById(id);
        findBooks.setName(request.getName());
        findBooks.setPageCount(request.getPageCount());
        findBooks.setPublishAddress(request.getPublishAddress());
        findBooks.setPublishedDate(request.getPublishedDate());
        findBooks.setAuthors(request.getAuthors());
        findBooks.setLink(request.getLink());
        findBooks.setBooksType(booksType);
        findBooks.setTypeId(request.getTypeId());
        findBooks.setInternetLink(request.getInternetLink());
        findBooks.setModifiedDate(new Date(System.currentTimeMillis()));
        findBooks.setStatus(request.getStatus());
        return booksRepository.save(findBooks);
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            booksRepository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found");
        }
    }

    @Override
    public List<Books> list() throws ApiException {
        return booksRepository.findAll();
    }
}
