package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Category;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.CategoryRequest;
import uz.nuu.repository.CategoryRepository;
import uz.nuu.service.CategoryService;

import java.util.List;
import java.util.UUID;

@Service
public class CategoryServiceIml implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceIml(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category create(Category request) {
        return categoryRepository.save(request);
    }

    @Override
    public Category getById(UUID id) throws DataNotFoundException {
        return categoryRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            categoryRepository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found exception");
        }
    }

    @Override
    public List<Category> list() throws ApiException {
        return categoryRepository.findAll();
    }

    @Override
    public Category update(CategoryRequest request, UUID id) {
        Category category = getById(id);
        category.setName(request.getName());
        return categoryRepository.save(category);
    }
}
