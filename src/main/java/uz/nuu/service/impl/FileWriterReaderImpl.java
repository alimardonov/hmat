package uz.nuu.service.impl;

import org.apache.commons.lang.RandomStringUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.exception.FileUploadException;
import uz.nuu.service.FileWriterReader;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Service
public class FileWriterReaderImpl implements FileWriterReader {

    private final String privateDocumentsFilePath = System.getProperty("user.dir") + "/static/file/documents";
    private static final Logger logger = LogManager.getLogger("ServiceLogger");
    private final String privateAvatarFilePath = System.getProperty("user.dir") + "/static/file/avatar";


    @Override
    public Boolean avatarImgWrite(MultipartFile multipartFile, String fileName, Date createdDate) throws FileUploadException {

        String path = privateAvatarFilePath + "/" +
                createDatePath(createdDate) + "/" +
                fileName;

        logger.info(path + ":" + multipartFile);

        return fileWrite(multipartFile, path);
    }

    public Boolean documentWrite(MultipartFile multipartFile, String documentType, String fileName, Date createdDate) throws FileUploadException {

        String path = privateDocumentsFilePath + "/" +
                documentType + "/" +
                createDatePath(createdDate) + "/" +
                fileName;

        logger.info(path + ":" + multipartFile);

        return fileWrite(multipartFile, path);
    }

    public Resource getDocumentsAsResource(String fileName, String documentType, Date createdDate) throws DataNotFoundException, MalformedURLException {

        String path = privateDocumentsFilePath + "/" +
                documentType + "/" +
                createDatePath(createdDate) + "/" +
                fileName;

        return getFileAsResource(path);
    }

    @Override
    public Resource getAvatarImgAsResource(String fileName, Date createdDate) throws DataNotFoundException {

        String path = privateAvatarFilePath + "/" +
                createDatePath(createdDate) + "/" +
                fileName;

        return getFileAsResource(path);

    }

    @Override
    public Boolean fileWrite(MultipartFile multipartFile, String path) throws FileUploadException {

        logger.info(path + ":" + multipartFile);
        File privateFile = new File(path);

        try {

            privateFile.getParentFile().mkdirs();
            privateFile.createNewFile();
            multipartFile.transferTo(privateFile);

            return true;

        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
            throw new FileUploadException("FILE_NOT_UPLOADED");
        }

    }

    @Override
    public Resource getFileAsResource(String path) throws DataNotFoundException {

        File file = new File(path);

        Resource resource = null;

        try {
            resource = new UrlResource(file.toPath().toUri());

            if (resource.exists()) {
                return resource;
            } else {
                throw new DataNotFoundException("File not Found");
            }

        } catch (MalformedURLException | DataNotFoundException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
            throw new DataNotFoundException("File not Found");
        }

    }

    @Override
    public String createDatePath(Date createdDate) {

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(createdDate);

        return calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH);
    }

    public String generateFileName() {
        return RandomStringUtils.random(
                10,
                "0987654321qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM");
    }

}
