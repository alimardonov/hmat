package uz.nuu.service.impl;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.nuu.entity.News;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.NewsRepository;
import uz.nuu.service.NewsService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Service
public class NewsServiceImpl implements NewsService {

    private final NewsRepository newsRepository;

    public NewsServiceImpl(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    @Override
    @Transactional
    public News create(News request) throws DataAlreadyExistsException {
        if (newsRepository.findByTitle(request.getTitle()) == null) {
            request.setCreatedDate(new Date(System.currentTimeMillis()));
            request.setModifiedDate(new Date(System.currentTimeMillis()));
            return newsRepository.save(request);
        } else {
            throw new DataAlreadyExistsException("This data already exists");
        }

    }

    @Override
    @Transactional
    public News getById(UUID id) throws DataNotFoundException {
        return newsRepository
                .findById(id)
                .orElseThrow(() -> new DataNotFoundException("Data not found id = " + id));
    }

    @Override
    public List<News> list() {
        return newsRepository.findAll();
    }

    @Override
    public News getByTitle(HashMap title) {
        return newsRepository.findByTitle(title);
    }

    @Override
    public News update(UUID id, News request) throws DataNotFoundException, DataAlreadyExistsException {
        try {
            News findNews = getById(id);
            if (getByTitle(request.getTitle()) != null) {
                throw new DataAlreadyExistsException("This data already exists");
            }
            findNews.setModifiedDate(new Date(System.currentTimeMillis()));
            findNews.setAvatar(request.getAvatar());
            findNews.setCategory(request.getCategory());
            findNews.setCategoryId(request.getCategoryId());
            findNews.setContent(request.getContent());
            findNews.setStatus(request.getStatus());
            findNews.setTitle(request.getTitle());
            return newsRepository.save(findNews);
        } catch (ConstraintViolationException e) {
            throw new DataAlreadyExistsException("This data already exists");
        }
    }

    @Override
    public Page<News> list(Pageable pageable) throws ApiException {
        return newsRepository.findAll(pageable);
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            newsRepository.deleteById(id);
            return true;
        } else {
            throw new DataNotFoundException("Data not found id = " + id);
        }
    }

    @Override
    public Page<News> getByCategory(Pageable pageable, UUID id) {
        return newsRepository.findAllByCategoryIdIs(id, pageable);
    }
}
