package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import uz.nuu.entity.Articles;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.ArticlesRequest;
import uz.nuu.repository.ArticlesRepository;
import uz.nuu.repository.TeachersRepository;
import uz.nuu.service.ArticlesService;

import java.util.List;
import java.util.UUID;

@Service
public class ArticlesServiceImpl implements ArticlesService {
    private final ArticlesRepository articlesRepository;
    private final TeachersRepository teachersRepository;

    public ArticlesServiceImpl(ArticlesRepository articlesRepository, TeachersRepository teachersRepository) {
        this.articlesRepository = articlesRepository;
        this.teachersRepository = teachersRepository;
    }

    @Override
    public Articles create(Articles request) throws DataAlreadyExistsException {
        return articlesRepository.save(request);
    }

    @Override
    public Articles getById(UUID id) throws DataNotFoundException {
        return articlesRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public Articles update(UUID id, ArticlesRequest request) throws DataNotFoundException, DataAlreadyExistsException {
        Articles findArticles = getById(id);
        findArticles.setBookUrl(request.getBookUrl());
        findArticles.setJournalName(request.getJournalName());
        findArticles.setmFacter(request.getmFacter());
        findArticles.setPageCount(request.getPageCount());
        findArticles.setPublishedDate(request.getPublishedDate());
        findArticles.setTeachersArticles(teachersRepository.findAllByIdIn(request.getTeachersIdList()));
        findArticles.setTitle(request.getTitle());
        findArticles.setType(request.getType());
        return articlesRepository.save(findArticles);
    }

    @Override
    public boolean delete(UUID id) throws DataNotFoundException {
        if (getById(id) != null) {
            articlesRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<Articles> list() throws ApiException {
        return articlesRepository.findAll();
    }
}
