package uz.nuu.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import uz.nuu.entity.FileDocumentTypeForFileDocumentsEntity;
import uz.nuu.repository.FileDocumentTypeForFileDocumentsRepository;
import uz.nuu.service.FileDocumentTypeForFileDocumentsService;

import java.util.List;

@Service
public class FileDocumentTypeForFileDocumentsServiceImpl implements FileDocumentTypeForFileDocumentsService {

    private static final Logger logger = LogManager.getLogger("ServiceLogger");

    private final FileDocumentTypeForFileDocumentsRepository repository;

    public FileDocumentTypeForFileDocumentsServiceImpl(FileDocumentTypeForFileDocumentsRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<FileDocumentTypeForFileDocumentsEntity> getAll() {

        return repository.findAll();
    }
}
