package uz.nuu.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.nuu.entity.Role;
import uz.nuu.entity.User;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.UserRepository;
import uz.nuu.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(User request) throws ApiException {
        if (!existsByEmail(request.getEmail()) && !existsByUsername(request.getUsername())) {
            request.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
            return userRepository.save(request);
        }
        throw new DataAlreadyExistsException("Email or username already exists");
    }

    @Override
    public User findById(Long id) throws ApiException {
        return userRepository.findById(id).orElseThrow(() -> new DataNotFoundException("id = " + id + " not found"));
    }

    @Override
    public Page<User> list(Pageable pageable) throws ApiException {
        return userRepository.findAll(pageable);
    }

    @Override
    public User update(Long id, User request) throws ApiException {
        User user = findById(id);
        if (user == null) {
            throw new DataNotFoundException("id = " + id + " not found");
        }
        user.setUsername(request.getUsername());
        user.setRoles(request.getRoles());
        user.setProvince(request.getProvince());
        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        user.setGender(request.getGender());
        user.setEmail(request.getEmail());
        user.setFullName(request.getFullName());
        return userRepository.save(user);
    }

    @Override
    public boolean delete(Long id) throws ApiException {
        userRepository.deleteById(id);
        return true;
    }

    @Override
    public List<User> list() throws ApiException {
        return userRepository.findAll();
    }

    @Override
    public Boolean existsByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean existsByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            return false;
        }
        return true;
    }

    @Override
    public List<Role> findRolesByUsername(String username) throws DataNotFoundException {
        User roles = userRepository.findByUsernameWithQuery(username);
        if (roles == null) {
            throw new DataNotFoundException("Data not found");
        }
        return roles.getRoles();
    }

    @Override
    public User findByName(String name) {
        if (name.equals("anonymousUser")){
            throw new DataNotFoundException("User not found");
        }
        return userRepository.findByUsername(name);
    }
}
