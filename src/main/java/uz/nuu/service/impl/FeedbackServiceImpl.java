package uz.nuu.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.nuu.entity.Feedback;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.repository.FeedbackRepository;
import uz.nuu.service.FeedbackService;

import java.util.List;


@Service
class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    @Transactional
    public Feedback create(Feedback request) {
        Feedback feedback = feedbackRepository.save(request);
        return feedback;
    }

    @Override
    public Feedback getById(Long id) throws DataNotFoundException {
        return feedbackRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Data not found"));
    }

    @Override
    public boolean delete(Long id) throws DataNotFoundException {
        feedbackRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Feedback> list() throws ApiException {
        return feedbackRepository.findAll();
    }
}
