package uz.nuu.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.entity.FileAvatar;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.exception.FileUploadException;
import uz.nuu.exception.InvalidDataException;
import uz.nuu.repository.FileAvatarRepository;
import uz.nuu.service.FileAvatarService;

import java.util.Date;
import java.util.UUID;

@Service
public class FileAvatarServiceImpl implements FileAvatarService {

    private static final Logger logger = LogManager.getLogger("ServiceLogger");

    private FileAvatarRepository fileAvatarRepository;
    private FileWriterReaderImpl fileWriterReader;

    public FileAvatarServiceImpl(FileAvatarRepository fileAvatarRepository) {
        super();
        fileWriterReader = new FileWriterReaderImpl();
        this.fileAvatarRepository = fileAvatarRepository;
    }

    @Override
    @Transactional
    public FileAvatar avatarUpload(MultipartFile multipartFile) throws FileUploadException, InvalidDataException {

        String originalFileName = multipartFile.getOriginalFilename();

        String contentType = null;

        if (multipartFile.getContentType().toLowerCase().equals("application/jpg") ||
                multipartFile.getContentType().toLowerCase().equals("image/jpg")) {
            contentType = ".jpg";
        }

        if (multipartFile.getContentType().toLowerCase().equals("application/jpeg") ||
                multipartFile.getContentType().toLowerCase().equals("image/jpeg")) {
            contentType = ".jpeg";
        }

        if (multipartFile.getContentType().toLowerCase().equals("application/png") ||
                multipartFile.getContentType().toLowerCase().equals("image/png")) {
            contentType = ".png";
        }

        if (contentType == null) {
            throw new InvalidDataException("Rasm jpeg,png formatda emas");
        }

        FileAvatar fileAvatarEntity = new FileAvatar();
        fileAvatarEntity.setOriginalName(originalFileName);
        fileAvatarEntity.setCreatedDate(new Date(System.currentTimeMillis()));
        fileAvatarEntity.setGeneratedName(fileWriterReader.generateFileName() + contentType);

        logger.info(fileAvatarEntity.toString());

        fileAvatarEntity = fileAvatarRepository.save(fileAvatarEntity);
        fileWriterReader.avatarImgWrite(multipartFile, fileAvatarEntity.getGeneratedName(), fileAvatarEntity.getCreatedDate());

        return fileAvatarEntity;
    }

    @Override
    @Transactional
    public Resource getAvatarImgAsResourceByID(UUID id) throws DataNotFoundException {

        FileAvatar fileAvatar = fileAvatarRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Avatar Not Found"));

        return fileWriterReader
                .getAvatarImgAsResource(
                        fileAvatar.getGeneratedName(),
                        fileAvatar
                                .getCreatedDate());
    }

    @Override
    @Transactional
    public Resource getAvatarImgAsResourceByGeneratedName(String fileName) throws DataNotFoundException {

        FileAvatar fileAvatar = fileAvatarRepository.findByGeneratedName(fileName)
                .orElseThrow(() -> new DataNotFoundException("Avatar Not Found"));
        ;
        return fileWriterReader
                .getAvatarImgAsResource(
                        fileAvatar.getGeneratedName(),
                        fileAvatar
                                .getCreatedDate());
    }
}
