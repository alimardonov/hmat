package uz.nuu.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.exception.FileUploadException;

import java.net.MalformedURLException;
import java.util.Date;

public interface FileWriterReader {

    Boolean avatarImgWrite(MultipartFile multipartFile, String fileName, Date createdDate) throws FileUploadException;

    Resource getAvatarImgAsResource(String fileName, Date createdDate) throws DataNotFoundException, MalformedURLException;

    Boolean fileWrite(MultipartFile multipartFile, String path) throws FileUploadException;

    Resource getFileAsResource(String path) throws DataNotFoundException;

    String createDatePath(Date createdDate);

    String generateFileName();
}
