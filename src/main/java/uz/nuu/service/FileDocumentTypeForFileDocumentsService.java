package uz.nuu.service;

import uz.nuu.entity.FileDocumentTypeForFileDocumentsEntity;

import java.util.List;

public interface FileDocumentTypeForFileDocumentsService {

    List<FileDocumentTypeForFileDocumentsEntity> getAll();
}
