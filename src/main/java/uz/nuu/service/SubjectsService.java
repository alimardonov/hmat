package uz.nuu.service;

import uz.nuu.entity.Subjects;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;
import java.util.UUID;

public interface SubjectsService {
    Subjects create(Subjects request);

    Subjects getById(UUID id) throws DataNotFoundException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<Subjects> list() throws ApiException;

    Subjects update(Subjects request, UUID id);

}
