package uz.nuu.service;

import uz.nuu.entity.Menu;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.HashMap;
import java.util.List;

public interface MenuService {
    Menu create(Menu request) throws DataAlreadyExistsException;

    Menu getById(Long id) throws DataNotFoundException;

    Menu update(Long id, Menu request) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(Long id) throws DataNotFoundException;

    List<Menu> list() throws ApiException;

    Menu findById(Long id) throws DataNotFoundException;

    Menu getByTitle(HashMap title);
}
