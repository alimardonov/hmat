package uz.nuu.service;

import uz.nuu.entity.Teachers;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;
import java.util.UUID;

public interface TeachersService {
    Teachers create(Teachers request) throws DataAlreadyExistsException;

    Teachers getById(UUID id) throws DataNotFoundException;

    Teachers update(UUID id, Teachers request) throws DataNotFoundException, DataAlreadyExistsException;

    boolean delete(UUID id) throws DataNotFoundException;

    List<Teachers> list() throws ApiException;

    List<Teachers> findAllByIdIn(List<UUID> idList);

}
