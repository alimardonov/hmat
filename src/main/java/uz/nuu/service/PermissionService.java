package uz.nuu.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.nuu.entity.Permission;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;

import java.util.List;

public interface PermissionService {
    Permission create(Permission request) throws DataAlreadyExistsException;

    Permission getById(Long id) throws DataNotFoundException;

    Page<Permission> list(Pageable pageable) throws ApiException;

    Permission update(Long id, Permission request) throws DataNotFoundException,DataAlreadyExistsException;

    boolean delete(Long id) throws DataNotFoundException;

    List<Permission> list() throws ApiException;

    List<Permission> findByIds(List<Long> ids);

    Permission getByTitle(String title);
}
