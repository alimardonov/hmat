package uz.nuu.payload;

import javax.validation.constraints.NotNull;
import java.util.List;

public class UserRequest {
    @NotNull
    private String username;

    private String email;

    @NotNull
    private String password;

    @NotNull
    private String fullName;

    private String gender;

    private String province;

    private String district;

    private List<Long> roles;

    public UserRequest(@NotNull String username, String email, @NotNull String password, @NotNull String fullName, String gender, String province, String district, List<Long> roles) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.gender = gender;
        this.province = province;
        this.district = district;
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public List<Long> getRoles() {
        return roles;
    }

    public void setRoles(List<Long> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UserRequest{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", gender='" + gender + '\'' +
                ", province='" + province + '\'' +
                ", district='" + district + '\'' +
                ", roles=" + roles +
                '}';
    }
}
