package uz.nuu.payload;

import java.util.HashMap;

public class CategoryRequest {
    private HashMap<String, String> name;
    private String title;

    public CategoryRequest(HashMap<String, String> name) {
        this.name = name;
    }

    public CategoryRequest(HashMap<String, String> name, String title) {
        this.name = name;
        this.title = title;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryRequest{" +
                "name='" + name + '\'' +
                '}';
    }
}
