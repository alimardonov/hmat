package uz.nuu.payload;

import java.util.HashMap;

public class MenuRequest {
    private HashMap<String,String> title;
    private Long parentId;

    public MenuRequest(HashMap<String, String> title,Long parentId) {
        this.title = title;
        this.parentId = parentId;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "MenuRequest{" +
                "title=" + title +
                ", menu=" + parentId +
                '}';
    }
}
