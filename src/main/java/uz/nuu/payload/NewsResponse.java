package uz.nuu.payload;

import uz.nuu.entity.Category;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class NewsResponse {

    private UUID id;

    private HashMap<String, String> title;

    private HashMap<String, String> content;

    private Category category;

    private UUID categoryId;

    private Boolean status;

    private String createdBy;

    private String avatar;

    private String modifiedBy;

    private Date createdDate;

    private Date modifiedDate;

    private boolean deleted;

    public NewsResponse() {
    }

    public NewsResponse(UUID id, HashMap<String, String> title, HashMap<String, String> content, Category category, UUID categoryId, Boolean status, String createdBy, String avatar, String modifiedBy, Date createdDate, Date modifiedDate, boolean deleted) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.category = category;
        this.categoryId = categoryId;
        this.status = status;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.deleted = deleted;
        this.avatar = avatar;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "NewsResponse{" +
                "id=" + id +
                ", title=" + title +
                ", content='" + content + '\'' +
                ", category=" + category +
                ", categoryId=" + categoryId +
                ", status=" + status +
                ", createdBy='" + createdBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", deleted=" + deleted +
                '}';
    }
}
