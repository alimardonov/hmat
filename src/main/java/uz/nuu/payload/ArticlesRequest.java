package uz.nuu.payload;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ArticlesRequest {
    private HashMap<String, String> title;
    private HashMap<String, String> journalName;
    private Long pageCount;
    private Date publishedDate;
    private String bookUrl;
    private String type;
    private String mFacter;
    private List<UUID> teachersIdList;

    public ArticlesRequest() {
    }

    public ArticlesRequest(HashMap<String, String> title, HashMap<String, String> journalName, Long pageCount, Date publishedDate, String bookUrl, String type, String mFacter, List<UUID> teachersIdList) {
        this.title = title;
        this.journalName = journalName;
        this.pageCount = pageCount;
        this.publishedDate = publishedDate;
        this.bookUrl = bookUrl;
        this.type = type;
        this.mFacter = mFacter;
        this.teachersIdList = teachersIdList;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getJournalName() {
        return journalName;
    }

    public void setJournalName(HashMap<String, String> journalName) {
        this.journalName = journalName;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getBookUrl() {
        return bookUrl;
    }

    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getmFacter() {
        return mFacter;
    }

    public void setmFacter(String mFacter) {
        this.mFacter = mFacter;
    }

    public List<UUID> getTeachersIdList() {
        return teachersIdList;
    }

    public void setTeachersIdList(List<UUID> teachersIdList) {
        this.teachersIdList = teachersIdList;
    }

    @Override
    public String toString() {
        return "ArticlesRequest{" +
                "title=" + title +
                ", journalName=" + journalName +
                ", pageCount=" + pageCount +
                ", publishedDate=" + publishedDate +
                ", bookUrl='" + bookUrl + '\'' +
                ", type='" + type + '\'' +
                ", mFacter='" + mFacter + '\'' +
                ", teachersIdList=" + teachersIdList +
                '}';
    }
}
