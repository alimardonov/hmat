package uz.nuu.payload;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class BooksRequest {
    private HashMap<String, String> name;
    private Long pageCount;
    private HashMap<String, String> publishAddress;
    private Date publishedDate;
    private HashMap<String, String> authors;
    private String link;
    private UUID typeId;
    private String internetLink;
    private Boolean status;
    private String img;

    public BooksRequest() {
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public HashMap<String, String> getPublishAddress() {
        return publishAddress;
    }

    public void setPublishAddress(HashMap<String, String> publishAddress) {
        this.publishAddress = publishAddress;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public HashMap<String, String> getAuthors() {
        return authors;
    }

    public void setAuthors(HashMap<String, String> authors) {
        this.authors = authors;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public UUID getTypeId() {
        return typeId;
    }

    public void setTypeId(UUID typeId) {
        this.typeId = typeId;
    }

    public String getInternetLink() {
        return internetLink;
    }

    public void setInternetLink(String internetLink) {
        this.internetLink = internetLink;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BooksRequest{" +
                "name=" + name +
                ", pageCount=" + pageCount +
                ", publishAddress=" + publishAddress +
                ", publishedDate=" + publishedDate +
                ", authors=" + authors +
                ", link='" + link + '\'' +
                ", typeId=" + typeId +
                ", internetLink='" + internetLink + '\'' +
                ", status=" + status +
                '}';
    }
}
