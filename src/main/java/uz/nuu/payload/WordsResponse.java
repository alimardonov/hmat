package uz.nuu.payload;

import java.util.HashMap;

public class WordsResponse {
    private Long id;
    private HashMap<String, String> name;

    public WordsResponse(Long id, HashMap<String, String> name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "WordsResponse{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
