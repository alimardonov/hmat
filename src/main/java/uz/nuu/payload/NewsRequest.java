package uz.nuu.payload;

import java.util.HashMap;
import java.util.UUID;

public class NewsRequest {

    private HashMap<String, String> title;

    private HashMap<String, String> content;

    private UUID categoryId;

    private Boolean status;

    private boolean deleted;

    private String avatar;

    public NewsRequest() {
    }

    public NewsRequest(HashMap<String, String> title, HashMap<String, String> content, UUID categoryId, String avatar, Boolean status, boolean deleted) {
        this.title = title;
        this.content = content;
        this.categoryId = categoryId;
        this.status = status;
        this.deleted = deleted;
        this.avatar = avatar;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "NewsRequest{" +
                ", title=" + title +
                ", content='" + content + '\'' +
                ", categoryId=" + categoryId +
                ", status=" + status +
                ", deleted=" + deleted +
                '}';
    }
}
