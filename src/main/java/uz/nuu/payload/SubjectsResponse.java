package uz.nuu.payload;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class SubjectsResponse {
    private UUID id;
    private HashMap<String, String> name;
    private HashMap<String, String> title;
    private HashMap<String, String> content;
    private String img;
    private List<String> teachersImg;

    public SubjectsResponse(UUID id, HashMap<String, String> name, HashMap<String, String> title, HashMap<String, String> content, String img, List<String> teachersImg) {
        this.id = id;
        this.name = name;
        this.title = title;
        this.content = content;
        this.img = img;
        this.teachersImg = teachersImg;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public List<String> getTeachersImg() {
        return teachersImg;
    }

    public void setTeachersImg(List<String> teachersImg) {
        this.teachersImg = teachersImg;
    }

    @Override
    public String toString() {
        return "SubjectsResponse{" +
                "id=" + id +
                ", name=" + name +
                ", title=" + title +
                ", content=" + content +
                ", img='" + img + '\'' +
                ", teachersImg=" + teachersImg +
                '}';
    }
}
