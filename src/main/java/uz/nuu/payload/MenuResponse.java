package uz.nuu.payload;

import uz.nuu.entity.Menu;

import java.util.HashMap;

public class MenuResponse {
    private Long id;
    private HashMap<String, String> title;
    private Menu menu;

    public MenuResponse(HashMap<String, String> title, Menu menu, Long id) {
        this.id = id;
        this.title = title;
        this.menu = menu;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MenuRequest{" +
                "title=" + title +
                ", menu=" + menu +
                '}';
    }
}
