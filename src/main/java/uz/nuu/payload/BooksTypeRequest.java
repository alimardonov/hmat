package uz.nuu.payload;

import java.util.HashMap;

public class BooksTypeRequest {

    private HashMap<String, String> name;
    private Boolean status;

    public BooksTypeRequest() {
    }

    public BooksTypeRequest(HashMap<String, String> name, Boolean status) {
        this.name = name;
        this.status = status;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BooksTypeRequest{" +
                "name=" + name +
                ", status=" + status +
                '}';
    }
}
