package uz.nuu.payload;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class SubjectsRequest {
    private HashMap<String, String> name;
    private HashMap<String, String> title;
    private HashMap<String, String> content;
    private String img;
    private List<UUID> teachersIdList;

    public SubjectsRequest(HashMap<String, String> name, HashMap<String, String> title, HashMap<String, String> content, String img, List<UUID> teachersIdList) {
        this.name = name;
        this.title = title;
        this.content = content;
        this.img = img;
        this.teachersIdList = teachersIdList;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getContent() {
        return content;
    }

    public void setContent(HashMap<String, String> content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public List<UUID> getTeachersIdList() {
        return teachersIdList;
    }

    public void setTeachersIdList(List<UUID> teachersIdList) {
        this.teachersIdList = teachersIdList;
    }

    @Override
    public String toString() {
        return "SubjectsRequest{" +
                "name=" + name +
                ", title=" + title +
                ", content=" + content +
                ", img='" + img + '\'' +
                ", teachersIdList=" + teachersIdList +
                '}';
    }
}
