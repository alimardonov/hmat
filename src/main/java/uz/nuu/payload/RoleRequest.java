package uz.nuu.payload;

import java.util.List;

public class RoleRequest {
    private String name;
    private String systemName;
    private String description;
    private boolean active;
    private List<Long> permissionIds;

    public RoleRequest() {
    }

    public RoleRequest(String name, String systemName, String description, boolean active, List<Long> permissionIds) {
        this.name = name;
        this.systemName = systemName;
        this.description = description;
        this.active = active;
        this.permissionIds = permissionIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }

    @Override
    public String toString() {
        return "RoleRequest{" +
                "name='" + name + '\'' +
                ", systemName='" + systemName + '\'' +
                ", description='" + description + '\'' +
                ", active=" + active +
                ", permissionIds=" + permissionIds +
                '}';
    }
}
