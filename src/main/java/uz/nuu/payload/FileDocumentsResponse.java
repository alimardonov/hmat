package uz.nuu.payload;

import java.util.UUID;

public class FileDocumentsResponse {

    private UUID id;
    private String documentType;
    private String originalName;
    private String generatedName;

    public FileDocumentsResponse() {
    }

    public FileDocumentsResponse(UUID id, String documentType, String originalName, String generatedName) {
        this.id = id;
        this.documentType = documentType;
        this.originalName = originalName;
        this.generatedName = generatedName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getGeneratedName() {
        return generatedName;
    }

    public void setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
    }
}
