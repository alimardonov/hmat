package uz.nuu.payload;

import uz.nuu.entity.Students;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class PrizesResponse {
    private UUID id;
    private HashMap<String, String> name;
    private Date givenTime;
    private Students students;
    private String givenBy;

    public PrizesResponse(UUID id, HashMap<String, String> name, Date givenTime, Students students, String givenBy) {
        this.id = id;
        this.name = name;
        this.givenTime = givenTime;
        this.students = students;
        this.givenBy = givenBy;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public Date getGivenTime() {
        return givenTime;
    }

    public void setGivenTime(Date givenTime) {
        this.givenTime = givenTime;
    }

    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }

    public String getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }

    @Override
    public String toString() {
        return "PrizesResponse{" +
                "id=" + id +
                ", name=" + name +
                ", givenTime=" + givenTime +
                ", students=" + students +
                ", givenBy='" + givenBy + '\'' +
                '}';
    }
}
