package uz.nuu.exception;

public class InvalidDataException extends ApiException {
    private static final long serialVersionUID = 1L;

    public InvalidDataException() {
    }

    public InvalidDataException(String message) {
        super(message);
    }
}
