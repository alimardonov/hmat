package uz.nuu.exception;

public class DataAlreadyExistsException extends ApiException {

    private static final long serialVersionUID = 1L;

    public DataAlreadyExistsException() {
    }

    public DataAlreadyExistsException(String message) {
        super(message);
    }
}
