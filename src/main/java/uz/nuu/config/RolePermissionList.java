package uz.nuu.config;

import org.springframework.stereotype.Component;
import uz.nuu.entity.Permission;
import uz.nuu.repository.RoleRepository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class RolePermissionList {

    private ConcurrentHashMap<String, List<String>> map = new ConcurrentHashMap<>();

    private final RoleRepository roleRepository;

    public RolePermissionList(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    public void init() {
        map.clear();

        roleRepository.findAll().forEach(roleEntity -> {
            if (roleEntity.getPermissions() != null) {
                map.put(roleEntity.getName(),
                        roleEntity
                                .getPermissions()
                                .stream()
                                .map(Permission::getName)
                                .collect(Collectors.toList()));
            }
        });
    }

    public boolean checkPermission(String role, String permission) {
        return map.containsKey(role) ? map.get(role).contains(permission) : false;
    }
}
