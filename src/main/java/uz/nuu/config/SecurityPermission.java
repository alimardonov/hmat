package uz.nuu.config;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component("SecurityPermission")
public class SecurityPermission {

    private final RolePermissionList rolePermissionList;

    public SecurityPermission(RolePermissionList rolePermissionList) {
        this.rolePermissionList = rolePermissionList;
    }

    public boolean hasPermission(String permission) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            return false;
        } else {
            String currentUserName = authentication.getName();
        }

        Collection<GrantedAuthority> list = (Collection<GrantedAuthority>) authentication.getAuthorities();

        for (GrantedAuthority item :
                list) {
            if (rolePermissionList.checkPermission(item.getAuthority(), permission)) {
                return true;
            }
        }
        return false;
    }
}
