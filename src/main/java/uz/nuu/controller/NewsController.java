package uz.nuu.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.NewsConverter;
import uz.nuu.entity.Category;
import uz.nuu.entity.News;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.ErrorResponse;
import uz.nuu.payload.NewsRequest;
import uz.nuu.payload.NewsResponse;
import uz.nuu.service.CategoryService;
import uz.nuu.service.impl.NewsServiceImpl;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/news")
@Api(value = "News endpoint")
public class NewsController {

    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL_PAGEABLE = "/pageable";
    private static final String GET_ALL_BY_CATEGORY_PAGEABLE = "/by/category/pageable";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private static final Logger logger = LogManager.getLogger("ServiceLogger");
    private final NewsServiceImpl newsService;
    private final CategoryService categoryService;
    private final NewsConverter newsConverter;

    public NewsController(NewsServiceImpl newsService, CategoryService categoryService) {
        this.newsService = newsService;
        this.categoryService = categoryService;
        this.newsConverter = new NewsConverter();
    }

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created news"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<NewsResponse> create(@RequestBody NewsRequest request) throws DataAlreadyExistsException {
        logger.info("Create new Permission : " + request);

        Category category = categoryService.getById(request.getCategoryId());

        NewsResponse response = newsConverter.convertFromEntity(
                newsService.create(newsConverter.convertFromRequest(request, category))
        );
        return ResponseEntity.ok(response);
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<NewsResponse> getById(@PathVariable UUID id) throws DataNotFoundException {
        logger.info("Get news by id: " + id);
        return ResponseEntity.ok(
                newsConverter.convertFromEntity(
                        newsService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL_PAGEABLE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity getAllPageable(@RequestParam int page, @RequestParam int size) throws ApiException {
        logger.info("get all news with page: " + page + " and size :" + size);
        return ResponseEntity.ok(
                newsConverter.convertFromNewsPage(
                        newsService.list(PageRequest.of(page, size))
                )
        );
    }

    @GetMapping(GET_ALL_BY_CATEGORY_PAGEABLE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity getAllByCategoryPageable(@RequestParam int page, @RequestParam int size, @RequestParam UUID categoryId) throws ApiException {
        logger.info("get all news with page: " + page + " and size :" + size);
        return ResponseEntity.ok(
                newsConverter.convertFromNewsPage(
                        newsService.getByCategory(PageRequest.of(page, size), categoryId)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity<List<NewsResponse>> getAll() throws ApiException {
        logger.info("Get all news");
        return ResponseEntity.ok(
                newsConverter.convertFromNewsList(newsService.list())
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity update(@RequestBody NewsRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {

        Category category = categoryService.getById(request.getCategoryId());

        News news = newsConverter.convertFromRequest(request, category);
        logger.info("update news with this param : " + request);
        return ResponseEntity.ok(
                newsConverter.convertFromEntity(newsService.update(id, news))
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorResponse.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws ApiException {
        logger.info("delete news with this id" + id);
        return ResponseEntity.ok(newsService.delete(id));
    }
}
