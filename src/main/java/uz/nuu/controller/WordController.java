package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.entity.Words;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.WordsRequest;
import uz.nuu.service.WordsService;

@RestController
@RequestMapping("/word")
public class WordController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final WordsService wordsService;

    public WordController(WordsService wordsService) {
        this.wordsService = wordsService;
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created permission"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody Words request) throws ApiException {
        logger.info("Create new Words : " + request);

        return ResponseEntity.ok(wordsService.create(request));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById words"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable Long id) throws ApiException {
        logger.info("Get words by this id : " + id);
        return ResponseEntity.ok(wordsService.getById(id));
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all words"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all words : ");
        return ResponseEntity.ok(wordsService.list());
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully words update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody WordsRequest request, @PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update words by this request : " + request + "id = " + id);
        return ResponseEntity.ok(
                wordsService.update(request, id)
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete word"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete role by this id : " + id);
        return ResponseEntity.ok(wordsService.delete(id));
    }
}
