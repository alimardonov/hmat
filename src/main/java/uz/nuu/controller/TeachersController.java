package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.TeachersConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.TeachersRequest;
import uz.nuu.service.TeachersService;

import java.util.UUID;

@RestController
@RequestMapping("/teachers")
public class TeachersController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final TeachersService teachersService;
    private final TeachersConverter teachersConverter;

    public TeachersController(TeachersService teachersService) {
        this.teachersService = teachersService;
        this.teachersConverter = new TeachersConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created Teacher"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody TeachersRequest request) throws ApiException {
        logger.info("Create new Teacher : " + request);
        return ResponseEntity.ok(
                teachersConverter.convertFromEntity(
                        teachersService.create(teachersConverter.convertFromRequest(request)
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById teacher"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get teacher by this id : " + id);
        return ResponseEntity.ok(
                teachersConverter.convertFromEntity(
                        teachersService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all teachers"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all teachers : ");
        return ResponseEntity.ok(
                teachersConverter.convertFromEntitiesList(
                        teachersService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully teachers update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody TeachersRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update teachers by this request : " + request + "id = " + id);
        return ResponseEntity.ok(
                teachersConverter.convertFromEntity(
                        teachersService.update(id, teachersConverter.convertFromRequest(request))
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete teachers"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete teachers by this id : " + id);
        return ResponseEntity.ok(teachersService.delete(id));
    }
}
