package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.FeedBackConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.FeedBackResponse;
import uz.nuu.payload.FeedbackRequest;
import uz.nuu.service.FeedbackService;

@RestController
@RequestMapping("/feedback")
public class FeedbackController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String DELETE = "/{id}";

    private final FeedbackService feedbackService;
    private final FeedBackConverter feedBackConverter;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
        this.feedBackConverter = new FeedBackConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created permission"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody FeedbackRequest request) throws ApiException {
        logger.info("Create new Feedback : " + request);
        FeedBackResponse feedBackResponse = feedBackConverter.convertFromEntity(feedbackService.create(feedBackConverter.convertFromRequest(request)));
        return ResponseEntity.ok(feedBackResponse
        );
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById role"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable Long id) throws ApiException {
        logger.info("Get Feedback by this id : " + id);
        return ResponseEntity.ok(
                feedBackConverter.convertFromEntity(
                        feedbackService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all role"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all feedback : ");
        return ResponseEntity.ok(
                feedBackConverter.convertFromEntitiesList(
                        feedbackService.list()
                )
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete role"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable Long id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete feedback by this id : " + id);
        return ResponseEntity.ok(feedbackService.delete(id));
    }
}
