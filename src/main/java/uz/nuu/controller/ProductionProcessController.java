package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.ProductionProcessConverter;
import uz.nuu.entity.Students;
import uz.nuu.entity.Teachers;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.ProductionProcessRequest;
import uz.nuu.service.impl.ProductionProcessServiceImpl;
import uz.nuu.service.impl.StudentsServiceImpl;
import uz.nuu.service.impl.TeachersImpl;

import java.util.UUID;

@RestController
@RequestMapping("/productionProcess")
public class ProductionProcessController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final ProductionProcessServiceImpl productionProcessService;
    private final TeachersImpl teachersServiceImpl;
    private final StudentsServiceImpl studentsServiceImpl;
    private final ProductionProcessConverter productionProcessConverter;

    public ProductionProcessController(ProductionProcessServiceImpl productionProcessService, TeachersImpl teachersServiceImpl, StudentsServiceImpl studentsServiceImpl) {
        this.productionProcessService = productionProcessService;
        this.teachersServiceImpl = teachersServiceImpl;
        this.studentsServiceImpl = studentsServiceImpl;
        this.productionProcessConverter = new ProductionProcessConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created Production Process"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody ProductionProcessRequest request) throws ApiException {
        logger.info("Create new Production Process : " + request);
        Teachers teachers = null;
        Students students = null;
        if (request.getTeacherId() != null) {
            teachers = teachersServiceImpl.getById(request.getTeacherId());
        }
        if (request.getStudentId() != null) {
            students = studentsServiceImpl.getById(request.getStudentId());
        }
        return ResponseEntity.ok(
                productionProcessConverter.convertFromEntity(
                        productionProcessService.create(productionProcessConverter.convertFromRequest(request, students, teachers)
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById Production Process"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get Production Process by this id : " + id);
        return ResponseEntity.ok(
                productionProcessConverter.convertFromEntity(
                        productionProcessService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all Production Process"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all Production Process : ");
        return ResponseEntity.ok(
                productionProcessConverter.convertFromEntitiesList(
                        productionProcessService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully Production Process update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody ProductionProcessRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update Production Process by this request : " + request + "id = " + id);

        Teachers teachers = null;
        Students students = null;

        if (request.getTeacherId() != null) {
            teachers = teachersServiceImpl.getById(request.getTeacherId());
        }

        if (request.getStudentId() != null) {
            students = studentsServiceImpl.getById(request.getStudentId());
        }
        return ResponseEntity.ok(
                productionProcessConverter.convertFromEntity(
                        productionProcessService.update(id, productionProcessConverter.convertFromRequest(request, students, teachers))
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete Production Process"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete Production Process by this id : " + id);
        return ResponseEntity.ok(productionProcessService.delete(id));
    }
}
