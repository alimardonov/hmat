package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.BooksTypeConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.BooksTypeRequest;
import uz.nuu.service.impl.BooksTypeServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/booksType")
public class BooksTypeController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final BooksTypeServiceImpl booksTypeService;
    private final BooksTypeConverter booksTypeConverter;

    public BooksTypeController(BooksTypeServiceImpl booksTypeService) {
        this.booksTypeService = booksTypeService;
        this.booksTypeConverter = new BooksTypeConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created booksType"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody BooksTypeRequest request) throws ApiException {
        logger.info("Create new BooksType : " + request);

        return ResponseEntity.ok(
                booksTypeConverter.convertFromEntity(
                        booksTypeService.create(booksTypeConverter.convertFromRequest(request)
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById booksType"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get BooksType by this id : " + id);
        return ResponseEntity.ok(
                booksTypeConverter.convertFromEntity(
                        booksTypeService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all BooksType"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all BooksType : ");
        return ResponseEntity.ok(
                booksTypeConverter.convertFromEntitiesList(
                        booksTypeService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully booksType update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody BooksTypeRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update BooksType by this request : " + request + "id = " + id);

        return ResponseEntity.ok(
                booksTypeConverter.convertFromEntity(
                        booksTypeService.update(id, booksTypeConverter.convertFromRequest(request))
                ));
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete booksType"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete booksType by this id : " + id);
        return ResponseEntity.ok(booksTypeService.delete(id));
    }
}
