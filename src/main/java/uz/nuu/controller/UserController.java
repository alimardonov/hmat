package uz.nuu.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.RoleConverter;
import uz.nuu.converter.UserConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.payload.UserRequest;
import uz.nuu.payload.UserResponse;
import uz.nuu.service.RoleService;
import uz.nuu.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL_PAGEABLE = "/pageable";
    private static final String GET_ALL = "/";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";
    private static final String GET_ROLES_BY_USERNAME = "/roles/{username}";
    private static final String GET_CURRENT_USER = "/current";

    private static final Logger logger = LogManager.getLogger("ServiceLogger");

    private final UserService userService;
    private final UserConverter userConverter;
    private final RoleService roleService;
    private final RoleConverter roleConverter;

    public UserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
        this.roleConverter = new RoleConverter();
        this.userConverter = new UserConverter();
    }


    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created user"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody UserRequest request) throws ApiException {
        logger.info("Create new user : " + request);
        return ResponseEntity.ok(
                userConverter.convertFromEntity(
                        userService.create(userConverter.convertFromRequest(request, roleService.findByIds(request.getRoles())))
                )
        );
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById user"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable Long id) throws ApiException {
        logger.info("Get user by id : " + id);
        return ResponseEntity.ok(
                userConverter.convertFromEntity(userService.findById(id))
        );
    }

    @GetMapping(GET_CURRENT_USER)
    @ApiOperation(value = "Get user properties")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list", response = UserResponse.class),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = UserResponse.class),
            @ApiResponse(code = 500, message = "Something went wrong", response = UserResponse.class)})
    public ResponseEntity getAccountMe() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return ResponseEntity.ok(userConverter.convertFromEntity(userService.findByName(authentication.getName())));
    }

    @GetMapping(GET_ROLES_BY_USERNAME)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getByUsername user"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getRolesByUsername(@PathVariable String username) throws ApiException {
        logger.info("Get user by username : " + username);
        return ResponseEntity.ok(
                roleConverter.convertFromRoleList(userService.findRolesByUsername(username))
        );
    }

    @GetMapping(GET_ALL_PAGEABLE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully all users with pageable"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getAllPageable(@RequestParam int page, @RequestParam int size) throws ApiException {
        logger.info("get all user by page : " + page + " and size : " + size);
        return ResponseEntity.ok(
                userConverter.convertFromPages(
                        userService.list(PageRequest.of(page, size))
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all user"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all user");
        return ResponseEntity.ok(
                userConverter.convertFromUserList(
                        userService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully update user"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody UserRequest request, @PathVariable Long id) throws ApiException {
        logger.info("Update user request: " + request + "id : " + id);
        return ResponseEntity.ok(
                userConverter.convertFromEntity(
                        userService.update(id, userConverter.convertFromRequest(request, roleService.findByIds(request.getRoles()))))
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete user"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable Long id) throws ApiException {
        logger.info("Delete user by id : " + id);
        return ResponseEntity.ok(userService.delete(id));
    }
}
