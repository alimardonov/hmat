package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.StudentsConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.StudentRequest;
import uz.nuu.service.impl.StudentsServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/student")
public class StudentsController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final StudentsServiceImpl studentsService;
    private final StudentsConverter studentsConverter;

    public StudentsController(StudentsServiceImpl studentsService) {
        this.studentsService = studentsService;
        this.studentsConverter = new StudentsConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created student"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody StudentRequest request) throws ApiException {
        logger.info("Create new student : " + request);

        return ResponseEntity.ok(
                studentsConverter.convertFromEntity(
                        studentsService.create(studentsConverter.convertFromRequest(request)
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById books"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get Student by this id : " + id);
        return ResponseEntity.ok(
                studentsConverter.convertFromEntity(
                        studentsService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all Student"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all Student : ");
        return ResponseEntity.ok(
                studentsConverter.convertFromEntitiesList(
                        studentsService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully student update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody StudentRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update student by this request : " + request + "id = " + id);

        return ResponseEntity.ok(
                studentsConverter.convertFromEntity(
                        studentsService.update(id, studentsConverter.convertFromRequest(request)))
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete student"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete student by this id : " + id);
        return ResponseEntity.ok(studentsService.delete(id));
    }
}
