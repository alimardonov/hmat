package uz.nuu.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.converter.ArticlesConverter;
import uz.nuu.exception.ApiException;
import uz.nuu.exception.DataAlreadyExistsException;
import uz.nuu.exception.DataNotFoundException;
import uz.nuu.payload.ArticlesRequest;
import uz.nuu.service.impl.ArticlesServiceImpl;
import uz.nuu.service.impl.TeachersImpl;

import java.util.UUID;

@RestController
@RequestMapping("/articles")
public class ArticlesController {
    private static final String CREATE = "/create";
    private static final String GET_BY_ID = "/{id}";
    private static final String GET_ALL = "";
    private static final String UPDATE = "/update/{id}";
    private static final String DELETE = "/{id}";

    private final ArticlesServiceImpl articlesService;
    private final TeachersImpl teachersServiceImpl;
    private final ArticlesConverter articlesConverter;

    public ArticlesController(ArticlesServiceImpl articlesService, TeachersImpl teachersServiceImpl) {
        this.articlesService = articlesService;
        this.teachersServiceImpl = teachersServiceImpl;
        this.articlesConverter = new ArticlesConverter();
    }

    private static final Logger logger = LogManager.getLogger("ControllerLogger");

    @PostMapping(CREATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully created books"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity create(@RequestBody ArticlesRequest request) throws ApiException {
        logger.info("Create new Articles : " + request);

        return ResponseEntity.ok(
                articlesConverter.convertFromEntity(
                        articlesService.create(articlesConverter.convertFromRequest(request, teachersServiceImpl.findAllByIdIn(request.getTeachersIdList()))
                        )
                ));
    }

    @GetMapping(GET_BY_ID)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully getById articles"),
            @ApiResponse(code = 500, message = "Something went wrong", response = ErrorController.class)})
    public ResponseEntity getById(@PathVariable UUID id) throws ApiException {
        logger.info("Get Articles by this id : " + id);
        return ResponseEntity.ok(
                articlesConverter.convertFromEntity(
                        articlesService.getById(id)
                )
        );
    }

    @GetMapping(GET_ALL)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully get all articles"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity getAll() throws ApiException {
        logger.info("Get all articles : ");
        return ResponseEntity.ok(
                articlesConverter.convertFromEntitiesList(
                        articlesService.list()
                )
        );
    }

    @PutMapping(UPDATE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully articles update"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity update(@RequestBody ArticlesRequest request, @PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Update articles by this request : " + request + "id = " + id);

        return ResponseEntity.ok(
                articlesConverter.convertFromEntity(
                        articlesService.update(id, request))
        );
    }

    @DeleteMapping(DELETE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully delete articles"),
            @ApiResponse(code = 500, message = "Something went error", response = ErrorController.class)})
    public ResponseEntity delete(@PathVariable UUID id) throws DataNotFoundException, DataAlreadyExistsException {
        logger.info("Delete articles by this id : " + id);
        return ResponseEntity.ok(articlesService.delete(id));
    }
}
