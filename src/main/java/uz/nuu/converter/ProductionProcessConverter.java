package uz.nuu.converter;

import uz.nuu.entity.ProductionProcess;
import uz.nuu.entity.Students;
import uz.nuu.entity.Teachers;
import uz.nuu.payload.ProductionProcessRequest;
import uz.nuu.payload.ProductionProcessResponse;

import java.util.List;
import java.util.stream.Collectors;

public class ProductionProcessConverter extends Converter<ProductionProcessResponse, ProductionProcess> {
    public ProductionProcessConverter() {
        super(fromDto -> new ProductionProcess(),
                fromEntity -> new ProductionProcessResponse(
                        fromEntity.getId(),
                        fromEntity.getCyName(),
                        fromEntity.getCyAddress(),
                        fromEntity.getCyPhoneNumber(),
                        fromEntity.getStartDate(),
                        fromEntity.getEndDate(),
                        fromEntity.getRating(),
                        fromEntity.getDocumentationLink(),
                        fromEntity.getTitle(),
                        fromEntity.getStudents(),
                        fromEntity.getTeachers()
                )
        );
    }

    public ProductionProcess convertFromRequest(ProductionProcessRequest request, Students students, Teachers teachers) {
        return new ProductionProcess(
                request.getCyName(),
                request.getCyAddress(),
                request.getCyPhoneNumber(),
                request.getStartDate(),
                request.getEndDate(),
                request.getRating(),
                request.getDocumentationLink(),
                request.getTitle(),
                students,
                students != null ? students.getId() : null,
                teachers,
                teachers != null ? teachers.getId() : null
        );
    }

    public List<ProductionProcessResponse> convertFromEntitiesList(List<ProductionProcess> processList) {

        return processList.stream()
                .map(fromEntity -> new ProductionProcessResponse(
                                fromEntity.getId(),
                                fromEntity.getCyName(),
                                fromEntity.getCyAddress(),
                                fromEntity.getCyPhoneNumber(),
                                fromEntity.getStartDate(),
                                fromEntity.getEndDate(),
                                fromEntity.getRating(),
                                fromEntity.getDocumentationLink(),
                                fromEntity.getTitle(),
                                fromEntity.getStudents(),
                                fromEntity.getTeachers()
                        )
                )
                .collect(Collectors.toList());
    }
}
