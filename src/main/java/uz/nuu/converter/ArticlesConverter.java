package uz.nuu.converter;

import uz.nuu.entity.Articles;
import uz.nuu.entity.Teachers;
import uz.nuu.payload.ArticlesRequest;
import uz.nuu.payload.ArticlesResponse;

import java.util.List;
import java.util.stream.Collectors;

public class ArticlesConverter extends Converter<ArticlesResponse, Articles> {
    public ArticlesConverter() {
        super(
                fromDto -> new Articles(),
                fromEntity -> new ArticlesResponse(
                        fromEntity.getId(),
                        fromEntity.getTitle(),
                        fromEntity.getJournalName(),
                        fromEntity.getPageCount(),
                        fromEntity.getPublishedDate(),
                        fromEntity.getBookUrl(),
                        fromEntity.getType(),
                        fromEntity.getmFacter(),
                        fromEntity.getTeachersArticles()
                )
        );
    }

    public Articles convertFromRequest(ArticlesRequest articlesRequest, List<Teachers> teachersList) {
        return new Articles(
                articlesRequest.getTitle(),
                articlesRequest.getJournalName(),
                articlesRequest.getPageCount(),
                articlesRequest.getPublishedDate(),
                articlesRequest.getBookUrl(),
                articlesRequest.getType(),
                articlesRequest.getmFacter(),
                teachersList
        );
    }

    public List<ArticlesResponse> convertFromEntitiesList(List<Articles> articlesList) {

        return articlesList.stream()
                .map(item -> new ArticlesResponse(
                        item.getId(),
                        item.getTitle(),
                        item.getJournalName(),
                        item.getPageCount(),
                        item.getPublishedDate(),
                        item.getBookUrl(),
                        item.getType(),
                        item.getmFacter(),
                        item.getTeachersArticles()

                ))
                .collect(Collectors.toList());
    }
}
