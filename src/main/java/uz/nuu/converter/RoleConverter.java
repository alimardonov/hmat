package uz.nuu.converter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import uz.nuu.entity.Permission;
import uz.nuu.entity.Role;
import uz.nuu.payload.PermissionResponse;
import uz.nuu.payload.RoleRequest;
import uz.nuu.payload.RoleResponse;

import java.util.List;
import java.util.stream.Collectors;

public class RoleConverter extends Converter<RoleResponse, Role> {
    public RoleConverter() {
        super(fromDto -> new Role(), fromEntity -> new RoleResponse(
                fromEntity.getId(),
                fromEntity.getName(),
                fromEntity.getSystemName(),
                fromEntity.getDescription(),
                fromEntity.getActive(),
                fromEntity.getPermissions() != null ?
                        fromEntity
                                .getPermissions()
                                .stream()
                                .map(permission -> new PermissionResponse(permission.getId(), permission.getName(), permission.getTitle()))
                                .collect(Collectors.toList()) : null
        ));
    }

    public Role convertFromRequest(RoleRequest roleRequest, List<Permission> permissionList) {
        return new Role(
                roleRequest.getName(),
                roleRequest.getSystemName(),
                roleRequest.getDescription(),
                roleRequest.isActive(),
                permissionList
        );
    }

    public List<RoleResponse> convertFromRoleList(List<Role> roles) {
        return roles
                .stream()
                .map(role -> new RoleResponse(
                        role.getId(),
                        role.getName(),
                        role.getSystemName(),
                        role.getDescription(),
                        role.getActive(),
                        role.getPermissions() != null ?
                                role
                                        .getPermissions()
                                        .stream()
                                        .map(permission -> new PermissionResponse(permission.getId(), permission.getName(), permission.getTitle())).collect(Collectors.toList()) : null)
                ).collect(Collectors.toList());
    }

    public Page<RoleResponse> convertFromRolePage(Page<Role> page) {
        return new PageImpl<>(
                page
                        .getContent()
                        .stream()
                        .map(role ->
                                new RoleResponse(
                                        role.getId(),
                                        role.getName(),
                                        role.getSystemName(),
                                        role.getDescription(),
                                        role.getActive(),
                                        role.getPermissions() != null ?
                                                role
                                                        .getPermissions()
                                                        .stream()
                                                        .map(permission -> new PermissionResponse(permission.getId(), permission.getName(), permission.getTitle())).collect(Collectors.toList()) : null)
                        ).collect(Collectors.toList()
                ), page.getPageable(), page.getTotalElements());
    }
}
