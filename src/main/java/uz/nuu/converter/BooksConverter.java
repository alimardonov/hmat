package uz.nuu.converter;

import uz.nuu.entity.Books;
import uz.nuu.entity.BooksType;
import uz.nuu.payload.BooksRequest;
import uz.nuu.payload.BooksResponse;

import java.util.List;
import java.util.stream.Collectors;

public class BooksConverter extends Converter<BooksResponse, Books> {
    public BooksConverter() {
        super(fromDto -> new Books(),
                fromEntity -> new BooksResponse(
                        fromEntity.getId(),
                        fromEntity.getName(),
                        fromEntity.getPageCount(),
                        fromEntity.getPublishAddress(),
                        fromEntity.getPublishedDate(),
                        fromEntity.getAuthors(),
                        fromEntity.getLink(),
                        fromEntity.getBooksType(),
                        fromEntity.getTypeId(),
                        fromEntity.getInternetLink(),
                        fromEntity.getCreatedDate(),
                        fromEntity.getModifiedDate(),
                        fromEntity.getCreatedBy(),
                        fromEntity.getModifiedBy(),
                        fromEntity.getStatus(),
                        fromEntity.getImg()
                )
        );
    }

    public Books convertFromRequest(BooksRequest request, BooksType type) {
        return new Books(
                request.getName(),
                request.getPageCount(),
                request.getPublishAddress(),
                request.getPublishedDate(),
                request.getAuthors(),
                request.getLink(),
                request.getTypeId(),
                request.getInternetLink(),
                request.getStatus(),
                request.getImg(),
                type
        );
    }

    public List<BooksResponse> convertFromEntitiesList(List<Books> booksList) {

        return booksList.stream()
                .map(fromEntity -> new BooksResponse(
                                fromEntity.getId(),
                                fromEntity.getName(),
                                fromEntity.getPageCount(),
                                fromEntity.getPublishAddress(),
                                fromEntity.getPublishedDate(),
                                fromEntity.getAuthors(),
                                fromEntity.getLink(),
                                fromEntity.getBooksType(),
                                fromEntity.getTypeId(),
                                fromEntity.getInternetLink(),
                                fromEntity.getCreatedDate(),
                                fromEntity.getModifiedDate(),
                                fromEntity.getCreatedBy(),
                                fromEntity.getModifiedBy(),
                                fromEntity.getStatus(),
                                fromEntity.getImg()
                        )
                )
                .collect(Collectors.toList());
    }
}
