package uz.nuu.converter;

import uz.nuu.entity.Category;
import uz.nuu.payload.CategoryRequest;
import uz.nuu.payload.CategoryResponse;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryConverter extends Converter<CategoryResponse, Category> {
    public CategoryConverter() {
        super(
                fromDto -> new Category(),
                fromEntity -> new CategoryResponse(
                        fromEntity.getId(),
                        fromEntity.getName()
                )
        );
    }

    public Category convertFromRequest(CategoryRequest request) {
        return new Category(request.getName());
    }

    public List<CategoryResponse> convertFromEntitiesList(List<Category> categoryList) {
        return categoryList.stream()
                .map(item -> new CategoryResponse(item.getId(), item.getName()))
                .collect(Collectors.toList());
    }
}
