package uz.nuu.converter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import uz.nuu.entity.Category;
import uz.nuu.entity.News;
import uz.nuu.payload.NewsRequest;
import uz.nuu.payload.NewsResponse;

import java.util.List;
import java.util.stream.Collectors;

public class NewsConverter extends Converter<NewsResponse, News> {
    public NewsConverter() {
        super(
                fromDto -> new News(),
                fromEntity -> new NewsResponse(
                        fromEntity.getId(),
                        fromEntity.getTitle(),
                        fromEntity.getContent(),
                        fromEntity.getCategory(),
                        fromEntity.getCategoryId(),
                        fromEntity.getStatus(),
                        fromEntity.getCreatedBy(),
                        fromEntity.getAvatar(),
                        fromEntity.getModifiedBy(),
                        fromEntity.getCreatedDate(),
                        fromEntity.getModifiedDate(),
                        fromEntity.isDeleted())
        );
    }

    public News convertFromRequest(NewsRequest request, Category category) {
        return new News(
                request.getTitle(),
                request.getContent(),
                category,
                request.getCategoryId(),
                request.getStatus(),
                request.getAvatar(),
                request.isDeleted()
        );
    }

    public List<NewsResponse> convertFromNewsList(List<News> list) {
        return list
                .stream()
                .map(item -> new NewsResponse(
                        item.getId(),
                        item.getTitle(),
                        item.getContent(),
                        item.getCategory(),
                        item.getCategoryId(),
                        item.getStatus(),
                        item.getCreatedBy(),
                        item.getAvatar(),
                        item.getModifiedBy(),
                        item.getCreatedDate(),
                        item.getModifiedDate(),
                        item.isDeleted()))
                .collect(Collectors.toList());
    }

    public Page<NewsResponse> convertFromNewsPage(Page<News> news) {
        return new PageImpl<>(news.getContent().stream().map(item -> new NewsResponse(item.getId(),
                item.getTitle(),
                item.getContent(),
                item.getCategory(),
                item.getCategoryId(),
                item.getStatus(),
                item.getCreatedBy(),
                item.getAvatar(),
                item.getModifiedBy(),
                item.getCreatedDate(),
                item.getModifiedDate(),
                item.isDeleted())).collect(Collectors.toList()), news.getPageable(), news.getTotalElements());
    }
}
