package uz.nuu.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "production_process")
@TypeDef(name = "json", typeClass = JsonBinaryType.class)
public class ProductionProcess {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "cyName", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> cyName;

    @Column(name = "cy_address", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> cyAddress;

    @Column(name = "cy_phone_number")
    private String cyPhoneNumber;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;

    @Column(name = "rating ")
    private String rating;

    @Column(name = "documentation_link")
    private String documentationLink;

    @Column(name = "title", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> title;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", updatable = false, insertable = false, nullable = false)
    private Students students;

    @Column(name = "student_id", nullable = false)
    private UUID studentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id", updatable = false, insertable = false, nullable = false)
    private Teachers teachers;

    @Column(name = "teacher_id", nullable = false)
    private UUID teacherId;

    public ProductionProcess() {
    }

    public ProductionProcess(HashMap<String, String> cyName, HashMap<String, String> cyAddress, String cyPhoneNumber, String startDate, String endDate, String rating, String documentationLink, HashMap<String, String> title, Students students, UUID studentId, Teachers teachers, UUID teacherId) {
        this.cyName = cyName;
        this.cyAddress = cyAddress;
        this.cyPhoneNumber = cyPhoneNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.rating = rating;
        this.documentationLink = documentationLink;
        this.title = title;
        this.students = students;
        this.studentId = studentId;
        this.teachers = teachers;
        this.teacherId = teacherId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getCyName() {
        return cyName;
    }

    public void setCyName(HashMap<String, String> cyName) {
        this.cyName = cyName;
    }

    public HashMap<String, String> getCyAddress() {
        return cyAddress;
    }

    public void setCyAddress(HashMap<String, String> cyAddress) {
        this.cyAddress = cyAddress;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDocumentationLink() {
        return documentationLink;
    }

    public void setDocumentationLink(String documentationLink) {
        this.documentationLink = documentationLink;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }

    public UUID getStudentId() {
        return studentId;
    }

    public void setStudentId(UUID studentId) {
        this.studentId = studentId;
    }

    public Teachers getTeachers() {
        return teachers;
    }

    public void setTeachers(Teachers teachers) {
        this.teachers = teachers;
    }

    public UUID getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(UUID teacherId) {
        this.teacherId = teacherId;
    }

    public String getCyPhoneNumber() {
        return cyPhoneNumber;
    }

    public void setCyPhoneNumber(String cyPhoneNumber) {
        this.cyPhoneNumber = cyPhoneNumber;
    }

    @Override
    public String toString() {
        return "ProductionProcess{" +
                "id=" + id +
                ", cyName=" + cyName +
                ", cyAddress=" + cyAddress +
                ", cy_phone_number='" + cyPhoneNumber + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", rating='" + rating + '\'' +
                ", documentationLink='" + documentationLink + '\'' +
                ", title=" + title +
                ", students=" + students +
                ", studentId=" + studentId +
                ", teachers=" + teachers +
                ", teacherId=" + teacherId +
                '}';
    }
}
