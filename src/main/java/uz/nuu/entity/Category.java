package uz.nuu.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "category")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Category {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "name", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> name;

    public Category() {
    }

    public Category(HashMap<String, String> name) {
        this.name = name;
    }

    public Category(UUID id, HashMap<String, String> name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "Long=" + id +
                ", name=" + name +
                '}';
    }
}
