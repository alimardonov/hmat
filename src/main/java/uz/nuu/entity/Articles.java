package uz.nuu.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "articles")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Articles {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "title", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> title;

    @Column(name = "journal_name", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> journalName;

    @Column(name = "page_count")
    private Long pageCount;


    @Column(name = "published_date")
    private Date publishedDate;

    @Column(name = "book_url")
    private String bookUrl;

    @Column(name = "type")
    private String type;

    @Column(name = "m_facter")
    private String mFacter;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "teachers_articles",
            joinColumns = @JoinColumn(name = "teacher_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "articles_id", referencedColumnName = "id")
    )
    @OrderBy
    @JsonIgnore
    private Collection<Teachers> teachersArticles;

    public Articles() {
    }

    public Articles(HashMap<String, String> title, HashMap<String, String> journalName, Long pageCount, Date publishedDate, String bookUrl, String type, String mFacter, Collection<Teachers> teachersArticles) {
        this.title = title;
        this.journalName = journalName;
        this.pageCount = pageCount;
        this.publishedDate = publishedDate;
        this.bookUrl = bookUrl;
        this.type = type;
        this.mFacter = mFacter;
        this.teachersArticles = teachersArticles;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public HashMap<String, String> getJournalName() {
        return journalName;
    }

    public void setJournalName(HashMap<String, String> journalName) {
        this.journalName = journalName;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getBookUrl() {
        return bookUrl;
    }

    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getmFacter() {
        return mFacter;
    }

    public void setmFacter(String mFacter) {
        this.mFacter = mFacter;
    }

    public Collection<Teachers> getTeachersArticles() {
        return teachersArticles;
    }

    public void setTeachersArticles(Collection<Teachers> teachersArticles) {
        this.teachersArticles = teachersArticles;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "id=" + id +
                ", title=" + title +
                ", journalName=" + journalName +
                ", pageCount=" + pageCount +
                ", publishedDate=" + publishedDate +
                ", bookUrl='" + bookUrl + '\'' +
                ", type='" + type + '\'' +
                ", mFacter='" + mFacter + '\'' +
                ", teachersArticles=" + teachersArticles +
                '}';
    }
}
