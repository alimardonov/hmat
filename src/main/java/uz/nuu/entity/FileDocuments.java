package uz.nuu.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "file_documents")
public class FileDocuments {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_type", referencedColumnName = "name", nullable = false, insertable = false, updatable = false)
    private FileDocumentTypeForFileDocumentsEntity documentTypeEntity;

    @Column(name = "document_type", nullable = false)
    private String documentType;

    @Column(name = "orginal_name", nullable = false)
    private String originalName;

    @Column(name = "genetared_name", nullable = false)
    private String generatedName;

    @Column(name = "created_date")
    private Date createdDate;

    public FileDocuments() {
    }

    public FileDocuments(String documentType, String originalName, String generatedName) {
        this.documentType = documentType;
        this.originalName = originalName;
        this.generatedName = generatedName;
        this.createdDate = new Date(System.currentTimeMillis());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public FileDocumentTypeForFileDocumentsEntity getDocumentTypeEntity() {
        return documentTypeEntity;
    }

    public void setDocumentTypeEntity(FileDocumentTypeForFileDocumentsEntity documentTypeEntity) {
        this.documentTypeEntity = documentTypeEntity;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getGeneratedName() {
        return generatedName;
    }

    public void setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "FileDocumentsEntity{" +
                "id=" + id +
                ", documentType='" + documentType + '\'' +
                ", originalName='" + originalName + '\'' +
                ", generatedName='" + generatedName + '\'' +
                '}';
    }
}
