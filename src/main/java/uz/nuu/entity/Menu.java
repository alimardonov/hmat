package uz.nuu.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.HashMap;

@Entity
@Table(name = "menu")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> title;

    @ManyToOne
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    private Menu parentMenu;

    public Menu() {
    }

    public Menu(HashMap<String, String> title, Menu parentMenu) {
        this.id = id;
        this.title = title;
        this.parentMenu = parentMenu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HashMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(HashMap<String, String> title) {
        this.title = title;
    }

    public Menu getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(Menu parentMenu) {
        this.parentMenu = parentMenu;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", title=" + title +
                ", parentMenu=" + parentMenu +
                '}';
    }
}
