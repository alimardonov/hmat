package uz.nuu.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "prizes")
@TypeDef(name = "json", typeClass = JsonBinaryType.class)
public class Prizes {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> name;

    @Column(name = "given_time")
    private Date givenTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", updatable = false, insertable = false, nullable = false)
    private Students students;

    @Column(name = "student_id", nullable = false)
    private UUID studentId;

    @Column(name = "given_by")
    private String givenBy;

    public Prizes() {
    }

    public Prizes( HashMap<String, String> name, Date givenTime, Students students, UUID studentId, String givenBy) {
        this.name = name;
        this.givenTime = givenTime;
        this.students = students;
        this.studentId = studentId;
        this.givenBy = givenBy;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public Date getGivenTime() {
        return givenTime;
    }

    public void setGivenTime(Date givenTime) {
        this.givenTime = givenTime;
    }

    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }

    public UUID getStudentId() {
        return studentId;
    }

    public void setStudentId(UUID studentId) {
        this.studentId = studentId;
    }

    public String getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }

    @Override
    public String toString() {
        return "Prizes{" +
                "id=" + id +
                ", name=" + name +
                ", givenTime=" + givenTime +
                ", students=" + students +
                ", studentId=" + studentId +
                ", givenBy='" + givenBy + '\'' +
                '}';
    }
}