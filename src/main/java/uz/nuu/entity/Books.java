package uz.nuu.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Entity
@Table(name = "books")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Books {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> name;

    @Column(name = "page_count")
    private Long pageCount;

    @Column(name = "publish_address", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> publishAddress;


    @Column(name = "published_date")
    private Date publishedDate;

    @Column(name = "authors", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private HashMap<String, String> authors;

    @Column(name = "link")
    private String link;

    @Column(name = "img")
    private String img;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id", updatable = false, insertable = false, nullable = false)
    private BooksType booksType;

    @Column(name = "type_id", nullable = false)
    private UUID typeId;

    @Column(name = "internet_link")
    private String internetLink;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "modifiedDate")
    private Date modifiedDate;

    @Column(name = "createdBy")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "status")
    private Boolean status;

    public Books() {
    }

    public Books(HashMap<String, String> name, Long pageCount, HashMap<String, String> publishAddress, Date publishedDate, HashMap<String, String> authors, String link, UUID typeId, String internetLink, Boolean status, String img, BooksType booksType) {
        this.name = name;
        this.pageCount = pageCount;
        this.publishAddress = publishAddress;
        this.publishedDate = publishedDate;
        this.authors = authors;
        this.link = link;
        this.typeId = typeId;
        this.internetLink = internetLink;
        this.status = status;
        this.img = img;
        this.booksType = booksType;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public HashMap<String, String> getName() {
        return name;
    }

    public void setName(HashMap<String, String> name) {
        this.name = name;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public HashMap<String, String> getPublishAddress() {
        return publishAddress;
    }

    public void setPublishAddress(HashMap<String, String> publishAddress) {
        this.publishAddress = publishAddress;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public HashMap<String, String> getAuthors() {
        return authors;
    }

    public void setAuthors(HashMap<String, String> authors) {
        this.authors = authors;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public BooksType getBooksType() {
        return booksType;
    }

    public void setBooksType(BooksType booksType) {
        this.booksType = booksType;
    }

    public UUID getTypeId() {
        return typeId;
    }

    public void setTypeId(UUID typeId) {
        this.typeId = typeId;
    }

    public String getInternetLink() {
        return internetLink;
    }

    public void setInternetLink(String internetLink) {
        this.internetLink = internetLink;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Books{" +
                "id=" + id +
                ", name=" + name +
                ", pageCount=" + pageCount +
                ", publishAddress=" + publishAddress +
                ", publishedDate=" + publishedDate +
                ", authors=" + authors +
                ", link='" + link + '\'' +
                ", booksType=" + booksType +
                ", typeId=" + typeId +
                ", internetLink='" + internetLink + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", createdBy='" + createdBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", status=" + status +
                '}';
    }
}